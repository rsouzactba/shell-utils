# README #

# Scripts Utils
Here you can find shell scripts utils.

# Collection

- slack.sh (to integrate with zabbix - unknown author) 

- ftp-to-host.sh (connect via ftp and get files directly)

- docker-compose.sh (script docker-composer initialization)

# Developer
Ricardo Souza - rsouzactba@hotmail.com