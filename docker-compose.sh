#!/bin/bash

#description "Ice and Nginx containers"
#author "Jon Brouse @jonbrouse github/jonbrouse"
#improvments "Ricardo Souza @rsouzactba https://bitbucket.org/rsouzactba/"

#start on (filesystem and started docker)
#stop on runlevel [!2345]

DOCKER_COMPOSE=`which docker-compose`
PATH_CONTAINER="/var/docker"

#### function to validate folder containers
validate_project(){
project=$1
ls -l $PATH_CONTAINER/$project/docker-compose.yml > /dev/null 2> /dev/null
if [ $? -eq 0 ];then
	status=0
   else
   	status=1
fi
}

#### function to start
start(){
#list containers folders
projects=$(ls -l $PATH_CONTAINER |awk '/dr/ {printf $9" "}')

#there are ou not a docker-compose.yml file
for container in $projects;
do
	### check if folder container are valid or not
	validate_project $container
	if [ $status -eq 0 ];then
		echo "starting $container...."
		echo $DOCKER_COMPOSE -f $PATH_CONTAINER/$container/docker-compose.yml up -d
		echo
	   else
	        break
        fi
done
}

#### function to stop all 
stop(){
projects=$(ls -l $PATH_CONTAINER |awk '/dr/ {printf $9" "}')

for container in $projects;
do
	### check if folder container are valid or not
	validate_project $container
	if [ $status -eq 0 ];then
    		echo "stopping $container...."
		echo  $DOCKER_COMPOSE -f $PATH_CONTAINER/$container/docker-compose.yml stop
		echo
	   else
		break
	fi
done
}

case "$1" in
    start)
	    start ;;
    stop)
	    stop ;;
    restart)
	    stop; start ;;
    reload|force-reload)
	    stop; start ;;
    status)
    	docker ps;;
    *)

    echo "Usage:  /etc/init.d/docker-compose.sh {start|stop|restart|reload|force-reload|status}" >&2
    exit 1
    ;;
esac

exit