#!/bin/sh

#
# connect to ftp and get many files
# by Ricardo Souza - rsouzactba@hotmail.com | v1 - nov/2018	
#

TODAY=`date +%Y%m%d`
HOUR=`TZ=UTC date +%H`
USER=username
PASSWD=your-pass
HOST=host-server-ftp

ftp -p -n $HOST <<SCRIPT
user $USER $PASSWD
binary
get file-name
quit
SCRIPT
