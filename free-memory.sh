#!/bin/sh

#script to release ram memory - by ricardo souza @rsouzactba
#tested in debian Linux 

mem_total="2040"
mem_min="400"
mem_free=`free -m | awk '/Mem:/ {print $4}'`

if [ "$mem_free" -le "$mem_min" ];then
        echo "releasing memory..."
        sync && echo 3 > /proc/sys/vm/drop_caches
    else
        exit
fi
